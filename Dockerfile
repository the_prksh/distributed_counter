
FROM erlang:19 as build

MAINTAINER Prakash Parmar <prakash.parmar@outlook.com>

RUN set -xe \
    && apt-get update \
    && apt-get install -y --no-install-recommends git openssl ca-certificates \ 
    && cd /usr/src \
    && git clone https://gitlab.com/parmar7725274/distributed_counter.git \
    && cd distributed_counter \
    && git checkout develop \
    && chmod 777 rebar3 \
    && make devrel \
    && cp -R _build/dev1/rel/distributed_counter /opt/ 
    
# Distributed Erlang Port Mapper
EXPOSE 4369

ENTRYPOINT ["/opt/distributed_counter/bin/distributed_counter"]
