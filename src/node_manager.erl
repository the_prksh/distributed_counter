
-module(node_manager).
-author("Prakash Parmar").

-behaviour(gen_statem).

%% API
-export([
         start_link/0,
         which_node/0,
         monitor_process/1,
         re_scan/0
        ]).

%% gen_statem callbacks
-export([
         init/1,
         terminate/3,
         code_change/4,
         callback_mode/0,
         
         connecting/3,
         ready/3
        ]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Creates a gen_statem process which calls Module:init/1 to
%% initialize. To ensure a synchronized start-up procedure, this
%% function does not return until Module:init/1 has returned.
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
  gen_statem:start_link({local, ?SERVER}, ?MODULE, [], []).

%%--------------------------------------------------------------------

which_node() ->
    catch gen_statem:call(node_manager, 'which_node?', 2000).

%%--------------------------------------------------------------------

re_scan()->
    catch gen_statem:call(node_manager, re_scan).

%%%===================================================================
%%% gen_statem callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_statem is started using gen_statem:start/[3,4] or
%% gen_statem:start_link/[3,4], this function is called by the new
%% process to initialize.
%%
%% @spec init(Args) -> {CallbackMode, StateName, State} |
%%                     {CallbackMode, StateName, State, Actions} |
%%                     ignore |
%%                     {stop, StopReason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
  {ok, connecting, #{}, 0}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_statem when it needs to find out 
%% the callback mode of the callback module.
%%
%% @spec callback_mode() -> atom().
%% @end
%%--------------------------------------------------------------------
callback_mode() ->
  state_functions.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% There should be one instance of this function for each possible
%% state name.  If callback_mode is state_functions, one of these
%% functions is called when gen_statem receives and event from
%% call/2, cast/2, or as a normal process message.
%%
%% @spec state_name(Event, From, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Actions} |
%%                   {stop, Reason, NewState} |
%%    				 stop |
%%                   {stop, Reason :: term()} |
%%                   {stop, Reason :: term(), NewData :: data()} |
%%                   {stop_and_reply, Reason, Replies} |
%%                   {stop_and_reply, Reason, Replies, NewState} |
%%                   {keep_state, NewData :: data()} |
%%                   {keep_state, NewState, Actions} |
%%                   keep_state_and_data |
%%                   {keep_state_and_data, Actions}
%% @end
%%--------------------------------------------------------------------

connecting(timeout, _content, _State) ->
%%     io:format("**** Connecting... *****~n"),
    scan();

connecting(cast, re_scan, _State) ->
    scan();

connecting({call, From}, 'which_node?', _State) ->
    {keep_state_and_data, [{reply, From, 'not_available'},
                           {timeout, 0, re_scan}       ]};

connecting(Event, Content, _State) ->
    io:format("~n[~w:~p] Got Event Type: ~p. Content : ~p, next state : same~n",
                                                        [?MODULE, ?LINE, Event, Content]),
    {keep_state_and_data, [{timeout, 0, re_scan}]}.

%%--------------------------------------------------------------------

ready({call, From}, 'which_node?', State) ->
    {keep_state_and_data, [ {reply, From, maps:get(up, State)}]};

ready(info, {nodeup, Node}, State) ->
    New_state = nodeup( Node, State),    
    io:format("~n[~w:~p] Node : ~p UP, Old State : ~p, New State : ~p~n",
                                                        [?MODULE, ?LINE, Node, State, New_state]),
    {keep_state, New_state};

ready(info, {nodedown, Node}, State) ->
    Reply = nodedown( Node, State),
    io:format("~n[~w:~p] Node : ~p Down, Old State : ~p, Reply : ~p~n",
                                                        [?MODULE, ?LINE, Node, State, Reply]),
    Reply;

ready(cast, re_scan, _State) ->
    scan();

ready(Type, Content, _State) ->
    io:format("~n[~w:~p] Got Event Type: ~p. Content : ~p, next state : same~n",
                                                        [?MODULE, ?LINE, Type, Content]),
    keep_state_and_data.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_statem when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_statem terminates with
%% Reason. The return value is ignored.
%%
%% @spec terminate(Reason, StateName, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _StateName, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, StateName, State, Extra) ->
%%                   {ok, StateName, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, StateName, State, _Extra) ->
  {ok, StateName, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

scan() ->
    Primary     = application:get_env(distributed_counter, primary_node,    'not_available'),
    Secondaries = application:get_env(distributed_counter, secondary_nodes, [] ),
    
    Up_node     = scan_for_active_node([Primary]++Secondaries),
    
    case Up_node of
        'not_available' ->
            {keep_state_and_data, [{timeout, 2000, re_scan}]};
  
        _ ->
            case Up_node of
                Primary -> ignore;
                _       -> erlang:spawn_link(?MODULE, monitor_process, [Primary])
            end,
            ok = net_kernel:monitor_nodes(true),
            {next_state, ready, #{  up         => Up_node,
                                    primary    => Primary,
                                    secondary  => Secondaries }}
    end.

%%--------------------------------------------------------------------

scan_for_active_node([]) ->
    'not_available';

scan_for_active_node([Node|Nodes]) ->
    case net_adm:ping(Node) of
        pong  -> Node;
        _pang -> scan_for_active_node(Nodes)
    end.
   
%%--------------------------------------------------------------------

nodedown(Node, Map) ->
    case maps:get(up, Map) of
        Node ->
            Primary     = maps:get(primary,   Map),
            Secondaries = maps:get(secondary, Map),
            
            case scan_for_active_node([Primary]++Secondaries) of
                'not_available' -> 
                    
                    case Node of
                        Primary -> 
                            erlang:spawn_link(?MODULE, monitor_process, [Node]);
                        _ -> ignore
                    end,
                    {next_state, connecting, Map, [{timeout, 0, re_scan}]};
                
                Active_node -> 
                    erlang:spawn_link(?MODULE, monitor_process, [Node]),
                    State = maps:put(up, Active_node, Map),
                    {keep_state, State}
            end;
        _ -> 
            {keep_state, Map}
    end.

%%--------------------------------------------------------------------

nodeup(Node, Map) ->   
    case maps:get(primary, Map) of
        Node -> maps:put(up, Node, Map);
        _    -> Map
    end.

%%--------------------------------------------------------------------

monitor_process(Node) ->
    case net_adm:ping(Node) of
        pong  -> connected;
        _pang -> 
            timer:sleep(5000),
            monitor_process(Node)
    end.    
