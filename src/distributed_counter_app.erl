%%%-------------------------------------------------------------------
%% @doc distributed_counter public API
%% @end
%%%-------------------------------------------------------------------

-module(distributed_counter_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-define(DEFAULT_PORT, 8080).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->

    Routes = [{ '_', [
                       {"/counter",           distributed_counter, [get]},
                       {"/counter/increment", distributed_counter, [increment]},
                       {"/counter/decrement", distributed_counter, [decrement]}
%%                        {"/counter/increment/:val", distributed_counter, [increment]},
%%                        {"/counter/decrement/:val", distributed_counter, [decrement]}
                     ]
               }],
    Dispatch = cowboy_router:compile(Routes),
    
    Port = application:get_env(distributed_counter, listen_port, ?DEFAULT_PORT),
    
    Trans_opts       = [{ip,   {0,0,0,0}}, {port, Port} ],
    Proto_opts       = #{env => #{dispatch => Dispatch} },
    
    {ok, _} = cowboy:start_clear(dc_http_listner,
                                 Trans_opts, 
                                 Proto_opts ),
    
    distributed_counter_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
