Distributed Counter 
=====

A CRDT (Conflict-free Replicated Data Types - High-level replicated data types that are designed to work correctly in the presence of concurrent updates and partial failures.) base `Counter`.

##### High LeveL Design :

![Design](docs/Distributed Counter.png)

##### To Setup :

>>>

0. Create a virtual network interface:

    ```shell
        $ docker network create --driver bridge my_bridge
    ```

1. Build docker image -

    ```shell
        $ docker build --no-cache -f ./Dockerfile -t distributed_counter .
    ```

2. Start distributed_counter node instance

    a) First instance 

    ```shell
        $ docker run -p 8080:8080 --network=my_bridge -it distributed_counter console
    ```
    
    Configure Primary and Secondary DB node connection 
    
    ```erlang
        
        application:set_env(distributed_counter, primary_node, 'antidote@antidote1').
        application:set_env(distributed_counter, secondary_nodes, ['antidote@antidote2']).
        
    ```
    
    b) Second instance
    
    ```shell
        $ docker run -p 8081:8080 --network=my_bridge -it distributed_counter console
    ```
    
    Configure Primary and Secondary DB node connection

    ```erlang

        application:set_env(distributed_counter, primary_node, 'antidote@antidote2').
        application:set_env(distributed_counter, secondary_nodes, ['antidote@antidote1']).
        
    ```

3. antidoteDB setup
    - Fetch AntidoteDB docker image by typing:
        ```shell
            $ docker pull antidotedb/antidote
        ```

    - Start two AntidoteDB node instances which will be called ‘antidote1’ and ‘antidote2’:
        ```shell

            $ docker run -i -t -d --name antidote1 --network my_bridge -e SHORT_NAME=true -e NODE_NAME=antidote@antidote1 antidotedb/antidote
            $ docker run -i -t -d --name antidote2 --network my_bridge -e SHORT_NAME=true -e NODE_NAME=antidote@antidote2 antidotedb/antidote

        ```

    - Connect to the console of one node:
        ```shell
            $ docker exec -it antidote1 /opt/antidote/bin/env attach
        ```

    - Run the following lines of code to connect the two antidoteDB nodes:
        ```erlang

            rpc:call(antidote@antidote1, inter_dc_manager, start_bg_processes, [stable]),
            rpc:call(antidote@antidote2, inter_dc_manager, start_bg_processes, [stable]),
            {ok, Desc1} = rpc:call(antidote@antidote1, inter_dc_manager, get_descriptor, []),
            {ok, Desc2} = rpc:call(antidote@antidote2, inter_dc_manager, get_descriptor, []),
            Descriptors = [Desc1, Desc2],
            rpc:call(antidote@antidote1, inter_dc_manager, observe_dcs_sync, [Descriptors]),
            rpc:call(antidote@antidote2, inter_dc_manager, observe_dcs_sync, [Descriptors]).

        ```

>>>
    
We now have an AntidoteDB cluster with two replicas and Two webservice node listening on Port `8080` and `8081`.

- To View counter value

>>>
```shell
    curl -X GET http://localhost:<PORT>/counter -H "Content-Type: application/text"
```

>>>

- To Increment Counter

>>>
```shell
    curl -X POST http://localhost:<PORT>/counter/increment -H "Content-Type: application/text"
```

>>>

- To Decrement Counter

>>>
```shell
    curl -X POST http://localhost:<PORT>/counter/decrement -H "Content-Type: application/text"
```

>>>

- Useful Docker command

>>>
- To Disconnect container from Network
```shell
   docker network disconnect my_bridge CONTAINER
```
CONTAINER = antidote1, antidote2
>>>

References :
* AntidoteDB - Introduction - https://www.youtube.com/watch?v=bGjdnBmX-oY
* https://www.antidotedb.eu/
* https://antidotedb.gitbook.io/documentation/
* http://www.erlang-factory.com/euc2016/annette-bieniusa

* https://github.com/ninenines/cowboy

* Docker Installation - https://docs.docker.com/install/
* https://docs.docker.com/v17.09/engine/userguide/networking/work-with-networks/
