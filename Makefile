#! /bin/bash

BASEDIR = $(shell pwd)
REBAR = $(BASEDIR)/rebar3

DEPS_PATH = ./_build/default/lib/*/ebin

export ROOTDIR=/home/$(USER)/lib

export RELX_REPLACE_OS_VARS=true

devrel:
	$(REBAR) as dev1 release
#	$(REBAR) as dev2 release

compile:
	$(REBAR) compile

clean:
	find -name "*~" -exec rm -rf {} \;
	rm -rf _build/*/rel
	$(REBAR) clean

dev1-console:
	$(BASEDIR)/_build/dev1/rel/distributed_counter/bin/distributed_counter console -mode interactive
	
#local_test:
#	$(REBAR) compile
#	rm -rf logs/*
#	mkdir -p logs
#	ct_run -dir test -suite es3_SUITE es3_chunk_SUITE -pa $(DEPS_PATH) -logdir logs
	
#dist_test:
#	$(REBAR) compile
#	rm -rf logs/*
#	mkdir -p logs
#	ct_run -dir test -suite distributed_test_SUITE -pa $(DEPS_PATH) -logdir logs 

#dialyzer:
#	$(REBAR) dialyzer
